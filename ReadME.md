# Requirements #

1. Provide an implementation of the interfaces which reads from JSON collection containing strings. 
2. Sorts the input strings by the "biggest" word contained in the string (strings with "bigger" word goes first). 
3. Feeds the output into JSON collection formatter, which prints JSON to response body. 
4. Rule for comparing words: words comparison rule: longer word is bigger, if words are of same length, the 'lexicographically bigger ' word is bigger. 
5. The sorting must be stable.
6. Some test coverage is expected

## Implementation ##

1. SentenceSortController main controller, path: /sentence/sort/word/descending

2. Request routed to SortSentenceService implementation, currently only one implemented: SortSentenceService

3. SortSentenceService execute call to SortAlgorithm,
currently only one implemented: StringLexicoDescendingSortAlgorithm