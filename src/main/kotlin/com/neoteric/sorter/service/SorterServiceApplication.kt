package com.neoteric.sorter.service

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SorterServiceApplication
fun main(args: Array<String>) {
    SpringApplication.run(SorterServiceApplication::class.java, *args)
}
