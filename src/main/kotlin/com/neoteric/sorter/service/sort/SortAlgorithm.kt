package com.neoteric.sorter.service.sort

interface SortAlgorithm<in T : Comparable<T>> {

    fun sort(values: List<T>): Map<String, String>
}