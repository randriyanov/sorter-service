package com.neoteric.sorter.service.sort

import org.springframework.stereotype.Service
import java.util.regex.Pattern

@Service
class StringLexicoDescendingSortAlgorithm : SortAlgorithm<String> {
    private val wordSplitRegexp = "[ !\"#$%&()*+,-./:;<=>?@\\[\\]^_`{|}~]+"

    override fun sort(values: List<String>) = values.map { sentence ->
        val biggest = sentence.split(Pattern.compile(wordSplitRegexp)).maxBy { it.length }.orEmpty()
        Pair(biggest, sentence)
    }.sortedWith(compareBy({ it.first.length }, { it.first }))
            .reversed().toMap()


}