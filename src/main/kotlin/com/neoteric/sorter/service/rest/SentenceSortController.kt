package com.neoteric.sorter.service.rest

import com.neoteric.sorter.service.model.request.BiggestWordSentenceSortRequest
import com.neoteric.sorter.service.service.SortSentenceService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sentence/sort")
class SentenceSortController(val service: SortSentenceService) {

    @PostMapping("/word/descending")
    fun profile(@RequestBody sortRequest: BiggestWordSentenceSortRequest) = service.getBiggestWordResponse(sortRequest)

}