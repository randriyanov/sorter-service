package com.neoteric.sorter.service.model.response

class BiggestWordSentenceSortResultItem(val string: String,
                                        val biggestWord: BiggestWordSentenceSortMetaData)