package com.neoteric.sorter.service.model.response

data class BiggestWordSentenceSortMetaData(val value: String,
                                           val length: Int)