package com.neoteric.sorter.service.model.response

import com.fasterxml.jackson.annotation.JsonCreator

data class BiggestWordSentenceSortResponse @JsonCreator constructor(val result: List<BiggestWordSentenceSortResultItem>)