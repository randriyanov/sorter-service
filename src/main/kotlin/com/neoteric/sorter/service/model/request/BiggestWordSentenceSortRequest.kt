package com.neoteric.sorter.service.model.request

import com.fasterxml.jackson.annotation.JsonCreator

class BiggestWordSentenceSortRequest @JsonCreator constructor(val strings: List<String>)