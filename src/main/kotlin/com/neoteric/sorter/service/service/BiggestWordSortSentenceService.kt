package com.neoteric.sorter.service.service

import com.neoteric.sorter.service.model.request.BiggestWordSentenceSortRequest
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortMetaData
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResponse
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResultItem
import com.neoteric.sorter.service.sort.SortAlgorithm
import org.springframework.stereotype.Service

@Service
class BiggestWordSortSentenceService(val sortAlgorithm: SortAlgorithm<String>) : SortSentenceService {

    override fun getBiggestWordResponse(input: BiggestWordSentenceSortRequest): BiggestWordSentenceSortResponse {
        return BiggestWordSentenceSortResponse(sortAlgorithm
                .sort(input.strings)
                .entries
                .map { mapToBiggestWordSentenceSortResponse(it.key, it.value) })
    }

    private fun mapToBiggestWordSentenceSortResponse(biggestWord: String,
                                                     sentence: String): BiggestWordSentenceSortResultItem {
        return BiggestWordSentenceSortResultItem(sentence,
                BiggestWordSentenceSortMetaData(biggestWord, biggestWord.length))
    }

}