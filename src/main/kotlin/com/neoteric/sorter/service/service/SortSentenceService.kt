package com.neoteric.sorter.service.service

import com.neoteric.sorter.service.model.request.BiggestWordSentenceSortRequest
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResponse

interface SortSentenceService {

    fun getBiggestWordResponse(input: BiggestWordSentenceSortRequest): BiggestWordSentenceSortResponse

}