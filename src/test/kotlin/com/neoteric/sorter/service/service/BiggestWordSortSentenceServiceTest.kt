package com.neoteric.sorter.service.service

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BiggestWordSortSentenceServiceTest {

    @Autowired
    lateinit var sortService: SortSentenceService


    @Test
    fun whenGivenBiggestWordRequestThenBiggestWordSentenceSortResponseReturn() {
        val sortResult = sortService.getBiggestWordResponse(getBiggestWordSentenceSortRequest())
        val expectedBiggestWord = getBiggestWordSentenceSortExpectedResponse().result[0].biggestWord;

        Assertions.assertTrue(sortResult.result[0].biggestWord == expectedBiggestWord);
    }
}