package com.neoteric.sorter.service.service

import com.neoteric.sorter.service.model.request.BiggestWordSentenceSortRequest
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortMetaData
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResponse
import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResultItem

fun getBiggestWordSentenceSortRequest() = BiggestWordSentenceSortRequest(
        listOf(
                "Sound boy proceed, to. blast: into/ the galaxy",
                "Go back rocket man into the sky you'll see",
                "Hear it all the time, come back rewind",
                "Aliens are watching up in the sky",
                "Sound boy process to blast into the galaxy",
                "No one gonna harm you",
                "They all want you to play I watch the birds of prey")
)

fun getBiggestWordSentenceSortExpectedResponse() = BiggestWordSentenceSortResponse(
        listOf(
                BiggestWordSentenceSortResultItem("Aliens are watching up in the sky",
                        BiggestWordSentenceSortMetaData("watching", 8)),
                BiggestWordSentenceSortResultItem("Sound boy process to blast into the galaxy",
                        BiggestWordSentenceSortMetaData("process", 7)),
                BiggestWordSentenceSortResultItem("Sound boy proceed, to. blast: into/ the galaxy", BiggestWordSentenceSortMetaData("proceed", 7)),
                BiggestWordSentenceSortResultItem("Go back rocket man into the sky you'll see", BiggestWordSentenceSortMetaData("rocket", 6)),
                BiggestWordSentenceSortResultItem("Hear it all the time, come back rewind",
                        BiggestWordSentenceSortMetaData("rewind", 6)),
                BiggestWordSentenceSortResultItem("They all want you to play I watch the birds of prey", BiggestWordSentenceSortMetaData("watch", 5)),
                BiggestWordSentenceSortResultItem("No one gonna harm you", BiggestWordSentenceSortMetaData("gonna", 5))
        )
)