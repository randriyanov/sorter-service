package com.neoteric.sorter.service.sort

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StringLexicoDescendingSortAlgorithmTest {

    var sortAlgorithm: SortAlgorithm<String> = StringLexicoDescendingSortAlgorithm();

    @Test
    fun `Given List With Sentence When Call Sort List with Sorted Sentence By Biggest Word descending Lexico return`() {
        val sortResult = sortAlgorithm.sort(sortedByBiggestWordLexicoTestData());
        assertEquals(sortResult, sortedByBiggestWordLexicoExpectResult())
    }
}