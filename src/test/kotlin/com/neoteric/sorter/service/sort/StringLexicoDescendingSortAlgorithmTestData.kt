package com.neoteric.sorter.service.sort

fun sortedByBiggestWordLexicoTestData() = listOf(
        "Sound boy proceed, to. blast: into/ the galaxy",
        "Go back rocket man into the sky you'll see",
        "Hear it all the time, come back rewind",
        "Aliens are watching up in the sky",
        "Sound boy process to blast into the galaxy",
        "No one gonna harm you",
        "They all want you to play I watch the birds of prey");

fun sortedByBiggestWordLexicoExpectResult() = mapOf(
        "watching" to "Aliens are watching up in the sky",
        "process" to "Sound boy process to blast into the galaxy",
        "proceed" to "Sound boy proceed, to. blast: into/ the galaxy",
        "rocket" to "Go back rocket man into the sky you'll see",
        "rewind" to "Hear it all the time, come back rewind",
        "watch" to "They all want you to play I watch the birds of prey",
        "gonna" to "No one gonna harm you"
)