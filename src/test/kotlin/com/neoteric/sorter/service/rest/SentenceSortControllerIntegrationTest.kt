package com.neoteric.sorter.service.rest

import com.neoteric.sorter.service.model.response.BiggestWordSentenceSortResponse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.postForObject
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension




@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SentenceSortControllerIntegrationTest(@Autowired val restTemplate: TestRestTemplate) {

    val sortRequestUrl = "/sentence/sort/word/descending"

    @Test
    fun `When request list with sentence then return ordered lexicographically sentence `() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val request = HttpEntity(sentenceSortRequestTestData(), headers)

        val responseResult = restTemplate.postForObject<BiggestWordSentenceSortResponse>(sortRequestUrl, request)

        assertTrue(responseResult!!.result[0].biggestWord.value == expectedFirstBiggestWord());
    }
}